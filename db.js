// Database Connection File
const Sequelize = require('sequelize');
const config = require('./config/config.json');

const db = new Sequelize('correspondence', 'root', '', {
    host: '127.0.0.1',
    dialect: 'mysql',
    config,
});

module.exports = db //export the db which is our Sequelize instance 
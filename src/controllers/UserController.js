const User = require('../models/User'); // import User Model (User Table)
const Sequelize = require('sequelize');

const allUsers = async (req, res) => {

    const users = await User.findAll({
        raw: true
    }).catch(err => {
        console.log(err);
    });

    await res.render('home', { users });
}
const userForm = async (req, res) => {
    await res.render('create');
}
const saveUser = async (req, res) => {
    const { name, email, phone } = req.body;
    console.log(phone)

    const result = await User.findAndCountAll({
        where: Sequelize.or(
            { name: name }, { email: email }, { phone: phone }
        )
    });

    if (result.count > 0) {
        res.send({
            "error": "Sorry, either username, email, or phone is taken",
            "user": result
        })
    }
    else {
        // Now you can add the new user record to the database:
        const newUser = await User.create({
            name, email, phone
        }).catch(err => {
            console.log(err)
        })

        res.send({
            "message": "User created successfully"
        })
    }


    // await res.redirect('/');
}

const editUser = async (req, res) => {
    const user = await User.findOne({
        where: {
            id: req.params.id
        },
        raw: true
    }).catch(err => console.log(err));

    res.render('edit', { user })
};

const updateUser = async (req, res) => {
    const { name, email, phone } = req.body;

    await User.update(
        {
            name, email, phone
        },
        {
            where: {
                id: req.params.id
            }
        }
    ).catch(err => console.log(err))

    res.redirect('/');
}

const viewUser = async (req, res) => {

    const user = await User.findOne(
        {
            where: {
                id: req.params.id
            },
            raw: true
        }
    ).catch(err => console.log(err))

    res.render('user', { user });
}
const deleteUser = async (req, res) => {

    const user = await User.destroy(
        {
            where: {
                id: req.params.id
            },
            raw: true
        }
    ).catch(err => console.log(err));


    res.redirect('/');
}


module.exports = {
    allUsers,
    userForm,
    saveUser,
    editUser,
    updateUser,
    viewUser,
    deleteUser
}
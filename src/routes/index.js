const express = require('express');
const router = express.Router(); // Create Express Router
// Now from UserController, import the allUsers function:
const { allUsers, userForm, saveUser, editUser, updateUser, viewUser, deleteUser } = require('../controllers/UserController');

// Now invoke the 'allUsers' function that will do the logic
router.get('/', allUsers);
router.get('/create', userForm);
router.post('/create', saveUser);
router.get('/edit/:id', editUser);
router.post('/update/:id', updateUser);
router.get('/user/:id', viewUser);
router.get('/delete/:id', deleteUser);

module.exports = router; //export the router
